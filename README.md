# si-ii-classe-inversee-backend- groupe6-22-23

## Command

Création

```shell
django-admin startproject <nom>
```

Launch

```shell
py manage.py runserver
```

Create polls app

```shell
python manage.py startapp expense
```

Apply migrations

```shell
python manage.py migrate
```

Create migrations

```shell
python manage.py makemigrations expense
```

```shell

python manage.py sqlmigrate expense 0001

```

Run migrations

```shell
python manage.py migrate
```