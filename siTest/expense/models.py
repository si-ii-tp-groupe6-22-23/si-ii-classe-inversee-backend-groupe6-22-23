from django.db import models
from django.shortcuts import get_object_or_404


class Item(models.Model):
    item_name = models.CharField(max_length=200)
    item_amount = models.IntegerField(default=0)
    item_category = models.ForeignKey('Category', on_delete=models.CASCADE)
    date_bought = models.DateTimeField('date bought')

    def __str__(self):
        return self.item_name

    def get_all_items(self):
        return Item.objects.all()

    def get_items_per_month(self, month):
        items_to_send = []
        for item in Item.objects.all():
            if item.date_bought.month == month:
                items_to_send.append(item)

        return items_to_send

    def get_amount_per_month(self, month):
        total = 0
        for item in Item.objects.all():
            if item.date_bought.month == month:
                total += item.item_amount

        return total

    def modify_amount(self, item_id, new_amount):
        selected_item = get_object_or_404(Item, pk=item_id)
        selected_item.item_amount = new_amount
        return selected_item


class Category(models.Model):
    name_category = models.CharField(max_length=200)

    def items_in_category(self):
        return Item.objects.filter(item_category=self)

    # total amount of items in category
    def total_amount(self):
        return sum([item.item_amount for item in self.items_in_category()])

    def get_category_id(self):
        return self.pk

    def __str__(self):
        return self.name_category
