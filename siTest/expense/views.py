from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Category, Item


# Static view
@csrf_exempt
def index(request):
    return HttpResponse(render(request, 'expense/index.html'))



@csrf_exempt
def amount_month(request):
    return HttpResponse(render(request, 'expense/amount_month.html', {
        'months': [{'month': 'January', 'index': 1}, {'month': 'February', 'index': 2}, {'month': 'March', 'index': 3},
                   {'month': 'April', 'index': 4}, {'month': 'May', 'index': 5}, {'month': 'June', 'index': 6},
                   {'month': 'July', 'index': 7}, {'month': 'August', 'index': 8}, {'month': 'September', 'index': 9},
                   {'month': 'October', 'index': 10}, {'month': 'November', 'index': 11},
                   {'month': 'December', 'index': 12}]}))


# Category
@csrf_exempt
def get_categories(request):
    categories = Category.objects.all()
    return HttpResponse(render(request, 'expense/categories.html', {'categories': categories}))


@csrf_exempt
def category_detail(request, category_id):
    # retrieve category object from database by id
    category = Category.objects.get(pk=category_id)
    # retrieve all items in category
    items = category.items_in_category()
    # retrieve total amount of items in category
    total_amount = category.total_amount()

    # print all items in category with amount and total amount
    return HttpResponse(render(request, 'expense/category_detail.html', {'category': category, 'items': items, 'total_amount': total_amount}))

@csrf_exempt
def delete_category(request, category_id):
    Category.objects.get(pk=category_id).delete()
    return get_categories(request)


@csrf_exempt
def add_category(request):
    category_name = request.POST['newName']
    c = Category(name_category=category_name)
    c.save()
    return get_categories(request)


# Items
# print all items in category
@csrf_exempt
def category_items(request, category_id):
    category = Category.objects.get(pk=category_id)
    return HttpResponse(category.items_in_category())

# print total amount of items per month
@csrf_exempt
def total_amount_month(request, month):
    items = Item.get_items_per_month(request, month)
    amount = Item.get_amount_per_month(request, month)
    return HttpResponse(render(request, 'expense/amount_per_month.html', {'items': items, 'amount': amount}))


@csrf_exempt
def add_item(request, category_id):
    amount = int(request.POST['amount'])
    name = request.POST['name']
    date = request.POST['date']
    c = Category.objects.get(pk=category_id)
    i = Item(item_name=name, item_amount=amount, item_category=c, date_bought=date)
    i.save()
    return category_detail(request, category_id)


@csrf_exempt
def delete_item(request, category_id, item_id):
    Item.objects.get(pk=item_id).delete()
    return category_detail(request, category_id)


@csrf_exempt
def modify_amount(request, category_id, item_id):
    new_amount = int(request.POST['newAmount'])
    print(new_amount)
    # retrieve item object from database by id
    item = Item.objects.get(pk=item_id)
    # modify amount of item
    item.item_amount = new_amount
    # save changes to database
    item.save()
    return category_detail(request, category_id)
