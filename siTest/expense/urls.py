from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('categories', views.get_categories, name='get_categories'),
    path('category/<int:category_id>/', views.category_detail, name='detail'),
    path('category/<int:category_id>/sum/', views.category_items, name='results'),
    path('amount-month', views.amount_month, name='amount_month'),
    path('total-amount-month/<int:month>/', views.total_amount_month, name='total_amount_month'),
    path('modify/<int:category_id>/<int:item_id>/', views.modify_amount, name='modify_amount'),
    path('category/delete/<int:category_id>', views.delete_category, name="delete_category"),
    path('category/add/', views.add_category, name="add_category"),
    path('category/<int:category_id>/add/', views.add_item, name="add_item"),
    path('category/<int:category_id>/delete/<int:item_id>', views.delete_item, name="delete_item")
]
